<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=banner',
    'username' => 'banner',
    'password' => 'banner',
    'charset' => 'utf8',
    'enableSchemaCache' => true
];
