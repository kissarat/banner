<?php

namespace app\helpers;


use PDO;
use Yii;
use yii\db\Connection;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class SQL {
    public static function execute($sql, $params = null) {
//        $out = $sql;
//        if ($params) {
//            foreach ($params as $key => $value) {
//                $out = str_replace($key, "'$value'", $out);
//            }
//        }
//        echo $out . "\n";
        $command = Yii::$app->db->createCommand($sql, $params);
        return $command->execute();
    }

    public static function queryObject($sql, $params = null) {
        return Yii::$app->db->createCommand($sql, $params)->pdoStatement->fetchObject();
    }

    public static function queryColumn($sql, $params = null) {
        return Yii::$app->db->createCommand($sql, $params)->queryColumn();
    }

    public static function queryCell($sql, $params = null) {
        return Yii::$app->db->createCommand($sql, $params)->pdoStatement->fetch(PDO::FETCH_COLUMN);
    }

    public static function querySingle($sql, $params = null, $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand($sql, $params)->pdoStatement->fetch($fetch_mode);
    }

    public static function queryAll($sql, $params = null, $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand($sql, $params)->queryAll($fetch_mode);
    }
}
