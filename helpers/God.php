<?php

namespace app\helpers;


use app\models\Settings;
use Yii;
use yii\i18n\Formatter;

class God {
    public static function generateRandomLiteral($length = 32) {
        $bytes = openssl_random_pseudo_bytes($length);
        return strtr(substr(base64_encode($bytes), 0, $length), '+/', 'A_');
    }

    public static function format() {
        return new Formatter();
    }

    public static function mail($email, $template, $params) {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params->email => Yii::$app->name])
            ->setSubject($params['subject'])
            ->setHtmlBody(Yii::$app->view->render("@app/mails/$template", $params))
            ->send();
    }

    public static function send($url, array $data, array $options = []) {

        $data = http_build_query($data);
        $curl = curl_init();
        curl_setopt_array($curl, array_merge([
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => [
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: ' . strlen($data),
                'Connetion: close'
            ]
        ], $options));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}
