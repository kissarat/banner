#!/bin/bash

mkdir runtime
chmod 777 runtime
mkdir web/assets
chmod 777 web/assets
mkdir web/upload
chmod 777 web/upload

chown :www-data runtime
chown :www-data web/assets
chown :www-data web/upload
