<?php
use app\models\Article;
use app\models\Image;
use app\widgets\ArticleView;
use yii\helpers\Html;

/** @var Image[] $center */
/** @var Image[] $left */
/** @var Image[] $right */

$this->title = 'Баннеры';
?>
<div class="image-index">
    <p>
        <?php
        if (!Yii::$app->user->getIsGuest()) {
            echo Html::a('Создать', ['edit'], ['class' => 'btn btn-primary']);
        }
        ?>
    </p>
</div>
