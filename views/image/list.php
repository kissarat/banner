<?php
use app\models\Image;
use yii\helpers\Html;

/** @var Image[] $models */
foreach($models as $model): ?>
    <div class="image">
        <?php
        $buttons = [];
        if ($model->name) {
            $buttons[] = Html::tag('span', $model->name);
        }
        if (!Yii::$app->user->getIsGuest()) {
            $buttons[] = Html::a('', ['/image/edit', 'id' => $model->id], ['class' => 'glyphicon glyphicon-edit']);
            $buttons[] = Html::a('', ['/image/delete', 'id' => $model->id], ['class' => 'glyphicon glyphicon-trash']);
        }
        if (count($buttons) > 0) {
            echo Html::tag('p', implode(' ', $buttons));
        }

        $image_options = [];
        foreach(['width', 'height'] as $name) {
            if (!empty($model->$name)) {
                $image_options[$name] = $model->$name;
            }
        }

        if (!empty($model->html)) {
            echo $model->html;
        }
        else {
            if ('swf' == $model->ext):
                ob_start();
                $image_options['type'] = 'application/x-shockwave-flash';
                $image_options['data'] = $model->getUrl();
                echo Html::beginTag('object', $image_options)
                ?>
                <param name="movie" value="<?= $model->getUrl() ?>"/>
                <param name="quality" value="high"/>
                <param name="scale" value="default"/>
                <?php
                echo Html::endTag('object');
                $img = ob_get_clean();
            else:
                $image_options['alt'] = $model->name;
                $img = Html::img($model->getUrl(), $image_options);
            endif;
            if ($model->url) {
                echo Html::a($img, $model->url, ['target' => '_blank']);
            } else {
                echo $img;
            }
        }
        ?>
    </div>
<?php endforeach ?>
