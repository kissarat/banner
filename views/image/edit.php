<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Баннер';

$types = [
    'center' => 'По центру',
    'left' => 'Слева',
    'right' => 'Справа',
];

$image_options = [
    'id' => 'preview',
    'class' => 'image'
];
if (empty($model->ext)) {
    $image_options['style'] = 'display: none';
}
?>
<div class="image-edit">
    <h1><?= $this->title ?></h1>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'banner')->checkbox() ?>
    <?= $form->field($model, 'type')->dropDownList($types) ?>
    <?= $form->field($model, 'number') ?>
    <?= $form->field($model, 'html')->textarea() ?>
    <?php if (empty($model->html)): ?>
        <div id="simple-image-fields">
            <?= $form->field($model, 'url') ?>
            <?= $form->field($model, 'width') ?>
            <?= $form->field($model, 'height') ?>

            <?= $form->field($model, 'image_url') ?>
            или
            <?= $form->field($model, 'file')->fileInput([
                'accept' => 'image/*'
            ]) ?>
            <?= Html::img($model->getUrl(), $image_options) ?>
            <hr />
        </div>
    <?php endif ?>
    <?= Html::submitButton('Сохранить') ?>

    <?php ActiveForm::end() ?>
</div>
