<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\Article;
use app\models\Image;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$banner_article = null;
if ('image' == Yii::$app->controller->id) {
    $banners = 'edit' == Yii::$app->controller->action->id
        ? ['center' => [],'left' => [],'right' => []]
        : Image::getImages();
    if ('index' == Yii::$app->controller->action->id) {
        $banner_article = Yii::$app->view->render('@app/views/article/view', [
            'model' => Article::findOne('main')
        ]);
    }
}
else {
    $banners = Image::getImages(true);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="generator" content="WordPress 3.6.1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $items = [
        ['label' => Yii::$app->user->getIsGuest() ? 'Главная' : 'Администрирования', 'url' => ['/home/index']]
    ];

    foreach (Article::getTitles('menu') as $id => $title) {
        $items[] = ['label' => $title, 'url' => ['/article/view', 'id' => $id]];
    }

    $items[] = Yii::$app->user->getIsGuest()
        ? ['label' => 'Вход', 'url' => ['/user/login']]
        : ['label' => 'Выход (' . Yii::$app->user->id . ')', 'url' => ['/user/logout']];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Alert::widget() ?>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?php
        if ($banner_article) {
            echo $banner_article;
        }
        ?>

        <div class="devil-center">
            <?= Yii::$app->view->render('@app/views/image/list', ['models' => $banners['center']]) ?>
        </div>

        <?= $content ?>
    </div>

    <div class="devil-left">
        <?= Yii::$app->view->render('@app/views/image/list', ['models' => $banners['left']]) ?>
    </div>

    <div class="devil-right">
        <?= Yii::$app->view->render('@app/views/image/list', ['models' => $banners['right']]) ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
