<?php
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Страницы';
?>
<div class="article-index">
    <h1><?= $this->title ?></h1>

    <p>
        <?php
        if (!Yii::$app->user->getIsGuest()) {
            echo Html::a('Создать', ['edit'], ['class' => 'btn btn-primary']);
        }
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'summary',
            ['class' => ActionColumn::className()]
        ]
    ]) ?>
</div>
