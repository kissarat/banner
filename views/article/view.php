<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

/* @var $model app\models\Article */

$this->title = $model->title;

foreach($model->getMetaTags() as $name => $content) {
    if (!empty($content)) {
        $this->registerMetaTag([
            'name' => $name,
            'content' => $content,
        ]);
    }
}
?>
<div class="article-view" data-id="<?= $model->id ?>">
    <h1><?= $model->title ?></h1>

    <p>
        <?php
        if (!Yii::$app->user->getIsGuest()) {
            echo Html::a('Редактировать', ['/article/edit', 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>
    </p>

    <div><?= $model->content ?></div>
</div>
