<?php

use dosamigos\ckeditor\CKEditor;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord) {
    $this->title = 'Создание страницы';
}
else {
    $this->title = $model->title . ' - редактирование';
}
?>

<div class="article-edit">

    <h1><?= $this->title ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?php
    if ($model->isNewRecord) {
        echo Html::tag('div', Url::to(['/page'], true) . '/' .
            Html::activeTextInput($model, 'id'), ['class' => 'form-group']);
    }
    ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'type')->dropDownList([
        '' => '',
        'menu' => 'меню'
    ]) ?>
    <?= $form->field($model, 'keywords') ?>
    <?= $form->field($model, 'summary')->textarea() ?>
    <?= $form->field($model, 'content')->widget(CKEditor::class, [
        'preset' => 'full'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
