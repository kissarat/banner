<?php
use yii\helpers\Html;

$this->title = 'Администрирование';
$items = [
    'Баннеры' => ['/image/index'],
    'Страницы' => ['/article/index'],
    'Изменить пароль' => ['/user/password'],
];
?>
<div class="home-admin">
    <h1><?= $this->title ?></h1>
    <?php
    foreach($items as $label => $url) {
        echo Html::tag('div', Html::a($label, $url));
    }
    ?>
</div>
