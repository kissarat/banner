<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменения пароля';
?>
<div class="user-password">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>

    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'repeat')->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary', 'name' => 'change-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
