<?php

namespace app\controllers;


use app\models\LoginForm;
use app\models\PasswordForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['password', 'logout'],
                'rules' => [
                    [
                        'actions' => ['password', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->getIsGuest()) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/home/admin']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionPassword()
    {
        $model = new PasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $user->setPassword($model->password);
            if ($user->save()) {
                return $this->redirect(['/home/admin']);
            }
        }
        return $this->render('password', [
            'model' => $model
        ]);
    }
}
