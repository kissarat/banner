<?php

namespace app\controllers;


use app\models\Image;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ImageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['edit'],
                'rules' => [
                    [
                        'actions' => ['edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index', Image::getImages());
    }

    public function actionEdit($id = null) {
        $model = $id ? $this->findModel($id) : new Image([
            'number' => 0,
            'banner' => false,
        ]);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            if ($model->save()) {
                $file = UploadedFile::getInstance($model, 'file');
                if ($file || !empty($model->image_url)) {
                    if ($model->getOldAttribute('ext')) {
                        $path = $model->getPath($model->getOldAttribute('ext'));
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                    $file_loaded = false;
                    if ($model->image_url) {
                        $path = parse_url($model->image_url);
                        $extension = null;
                        if (preg_match('|\.(\w+)$|', $path['path'], $extension)) {
                            $model->ext = strtolower($extension[1]);
                            $content = file_get_contents($model->image_url);
                            if ($content) {
                                file_put_contents($model->getPath(), $content);
                                $file_loaded = true;
                            }
                        }
                    } else {
                        $model->ext = $file->getExtension();
                        $path = $model->getPath();
                        if (move_uploaded_file($file->tempName, $path)) {
                            $file_loaded = true;
                        }
                    }

                    if ($file_loaded) {
                        $model->save();
                        $transaction->commit();
                        return $this->redirect(['index']);
                    } else {
                        Yii::$app->session->addFlash('error', 'Невозможно сохранить файл');
                    }
                } elseif (!$model->isNewRecord) {
                    $model->save();
                    $transaction->commit();
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->addFlash('error', 'Файл не загружен');
                }
            }
        }
        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        if ($model->delete()) {
            if (file_exists($model->getPath())) {
                unlink($model->getPath());
            }
            Yii::$app->session->addFlash('success', 'Баннер удален');
        }
        else {
            Yii::$app->session->addFlash('error', 'Невозможно удалить баннер');
        }
        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return Image
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Баннер не найден');
        }
    }
}
