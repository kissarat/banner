<?php

namespace app\controllers;

use app\models\Article;
use app\widgets\ArticleView;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class ArticleController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'edit'],
                'rules' => [
                    [
                        'actions' => ['index', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionIndex() {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Article::find()->orderBy([
                    'number' => SORT_DESC,
                    'time' => SORT_DESC,
                ]),
            ])
        ]);
    }

    public function actionCreate() {
        return $this->redirect(['edit']);
    }

    public function actionUpdate($id) {
        return $this->redirect(['edit', 'id' => $id]);
    }

    public function actionEdit($id = null) {
        $model = $id ? $this->findModel($id) : new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return Article
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Странице не найдена');
        }
    }

//    public function actionExport() {
//        /* @var \app\components\Backup $backup */
//        $backup = Yii::$app->backup;
//        return $this->redirect($backup->compress('article', [
//            'article.sql' => $backup->export('article', ['id', 'name', 'title', 'keywords', 'summary', 'content'], true),
//        ]));
//    }
}
