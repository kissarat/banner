<?php

namespace app\models;


use app\helpers\God;
use app\helpers\SQL;
use Exception;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\User as WebUser;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 *
 * @property string id
 * @property string name
 * @property string auth
 * @property string email
 * @property string code
 * @property string hash
 */
class User extends ActiveRecord implements IdentityInterface {

    const BLOCKED = 0;
    const ADMIN = 1;
    const PLAIN = 2;
    const MANAGER = 3;
    const TEAM = 4;

    const PERFECT = '/^E\d{7,8}$/';
    const NIX = '/^E\d+$/';

    private $_password;
    private $_info;
    public $repeat;
    public $adult;
    public $agreement;
    public $image;
    public $delete_avatar;

    public static function primaryKey() {
        return ['id'];
    }

    public static function statuses() {
        return [
            User::BLOCKED => Yii::t('app', 'Blocked'),
            User::ADMIN => Yii::t('app', 'Admin'),
            User::PLAIN => Yii::t('app', 'Registered'),
            User::MANAGER => Yii::t('app', 'Manager'),
            User::TEAM => Yii::t('app', 'Team')
        ];
    }

    public static $events = [
        'login' => 'Вход',
        'logout' => 'Выход',
        'login_fail' => 'Неудачная попытка входа',
    ];

    public function traceable() {
        return ['status', 'email', 'account'];
    }

    public function url() {
        return ['user/view', 'name' => $this->name];
    }

    public function rules() {
        return [
            ['id', 'string'],
            [['id', 'email'], 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => 'Логин',
            'email' => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * @param string $id
     * @return User
     */
    public static function findIdentity($id) {
        return parent::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new Exception('Not implemented findIdentityByAccessToken');
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->auth;
    }

    public function validateAuthKey($authKey) {
        return $authKey == $this->auth;
    }

    public function validatePassword($password) {
        return password_verify($password, $this->hash);
    }

    public function getPassword() {
        return $this->_password;
    }

    public function setPassword($value) {
        $this->hash = password_hash($value, PASSWORD_DEFAULT);
        $this->_password = $value;
    }

    public function generateAuthKey() {
        $this->auth = Yii::$app->security->generateRandomString(64);
    }

    public function generateCode() {
        $this->code = Yii::$app->security->generateRandomString(64);
    }

    public function __toString() {
        return $this->id;
    }

    public function sendEmail($params, $template = 'basic') {
        return God::mail($this->email, $template, $params);
    }
}
