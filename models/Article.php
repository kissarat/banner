<?php

namespace app\models;
use app\helpers\SQL;
use PDO;
use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 *
 * @property string $id
 * @property string $title
 * @property string $keywords
 * @property string $summary
 * @property string $content
 * @property string $type
 * @property string $number
 * @property string $time
 */
class Article extends ActiveRecord
{
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'type'], 'string'],
            ['title', 'string', 'min' => 3, 'max' => 255],
            ['keywords', 'string', 'min' => 3, 'max' => 192],
            ['summary', 'string', 'min' => 3, 'max' => 192],
            ['content', 'string', 'min' => 20],
            [['type', 'keywords', 'summary', 'content'], 'default', 'value' => null]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'type' => 'Показывать в',
            'keywords' => 'Ключевые слова',
            'summary' => 'Описание',
            'content' => 'Текст',
        ];
    }

    public function getMetaTags() {
        $tags = [];
        if ($this->keywords) {
            $tags['keywords'] = $this->keywords;
        }
        if ($this->summary) {
            $tags['description'] = $this->summary;
        }
        return $tags;
    }

    public static function getTitles($type) {
        return SQL::queryAll('SELECT id, title FROM article WHERE type = :type ORDER BY number DESC, id', [
            ':type' => $type
        ], PDO::FETCH_KEY_PAIR);
    }
}
