<?php

namespace app\models;


use yii\base\Model;

class PasswordForm extends Model
{
    public $password;
    public $repeat;

    public function rules()
    {
        return [
            [['password', 'repeat'], 'required'],
            ['repeat', 'compare', 'compareAttribute' => 'password']
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'repeat' => 'Повторите пароль',
        ];
    }
}
