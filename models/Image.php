<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @property string name
 * @property string url
 * @property string ext
 * @property string type
 * @property integer number
 */
class Image extends ActiveRecord
{
    public $file;
    public $image_url;

    public function rules()
    {
        return [
            [['number', 'banner'], 'required'],
            [['number', 'width', 'height'], 'integer'],
            ['banner', 'boolean'],
            [['html', 'image_url'], 'string'],
            ['name', 'string'],
//            ['ext', 'match', 'pattern' => '|^[a-z]$|'],
            [['url', 'ext'], 'string'],
            [['name', 'html', 'ext', 'url', 'width', 'height'], 'default', 'value' => null],
            ['number', 'default', 'value' => 0],
            ['banner', 'default', 'value' => false],
            ['type', 'in', 'range' => ['center', 'left', 'right']]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'banner' => 'показывать всюда',
            'url' => 'Ссылка',
            'type' => 'Позиция',
            'number' => 'Номер',
            'html' => 'HTML',
            'width' => 'Ширина',
            'height' => 'Высота',
            'image_url' => 'Загрузить с сайта',
            'file' => 'Файл',
        ];
    }

    public function getPath($ext = null) {
        if (empty($ext)) {
            $ext = $this->ext;
        }
        return Yii::getAlias("@app/web/upload/$this->id.$ext");
    }

    public function getUrl($ext = null) {
        if (empty($ext)) {
            $ext = $this->ext;
        }
        return empty($ext) ? null : Yii::getAlias("@web/upload/$this->id.$ext");
    }

    public static function getImages($banner = false) {
        $query = static::find()->orderBy([
            'number' => SORT_ASC,
            'id' => SORT_ASC,
        ]);

        if ($banner) {
            $query->where('banner');
        }

        $images = [
            'center' => [],
            'left' => [],
            'right' => [],
        ];
        foreach($query->all() as $model) {
            /** @var Image $model */
            $images[$model->type][] = $model;
        }
        return $images;
    }
}
