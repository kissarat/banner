function on(target, name, call) {
    if (!call) {
        call = name;
        name = 'click';
    }
    if ('string' == typeof target) {
        target = document.querySelectorAll(target);
        for (var i = 0; i < target.length; i++)
            target[i].addEventListener(name, call);
    }
    else
        target.addEventListener(name, call);
}

function $id(id) {
    return document.getElementById(id)
}

function each(array, call) {
    Array.prototype.forEach.call(array, call);
}

function $all(selector) {
    return document.querySelectorAll(selector)
}

function $each(selector, call) {
    if ('string' == typeof selector) {
        selector = $all(selector);
    }
    return each(selector, call);
}

Object.defineProperty(Element.prototype, 'visible', {
    get: function () {
        return 'none' != this.style.display
    },
    set: function (value) {
        if (value) {
            this.style.removeProperty('display');
        }
        else {
            this.style.display = 'none';
        }
    }
});

on('[type=file]', 'change', function () {
    var reader = new FileReader();
    var image = $id('preview');
    image.style.removeProperty('display');
    reader.onload = function (e) {
        image.src = e.target.result;
    };
    reader.readAsDataURL(this.files[0]);
});

on(window, 'load', function() {
    var fields_container = $id('simple-image-fields');
    if (fields_container) {
        on('textarea', 'change', function () {
            fields_container.visible = !this.value;
        });
    }
});
