<?php

namespace app\widgets;


use app\models\Article;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class ArticleView extends Action
{
    public function run($id) {
        if (empty($model = Article::findOne($id))) {
            throw new NotFoundHttpException('Странице не найдена');
        }

        return $this->controller->render('@app/views/article/view', [
            'model' => $model,
        ]);
    }
}
